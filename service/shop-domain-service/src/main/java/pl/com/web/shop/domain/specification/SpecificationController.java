package pl.com.web.shop.domain.specification;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import pl.com.bit.common.security.api.context.WithUserContext;
import pl.com.bit.domain.domain.api.SpecificationsApi;
import pl.com.bit.domain.domain.api.model.SpecificationCreateRequest;
import pl.com.bit.domain.domain.api.model.SpecificationDetails;
import pl.com.bit.domain.domain.api.model.SpecificationUpdateRequest;

import java.util.UUID;

import static pl.com.bit.common.security.api.UserContext.getIdentityOrThrow;

@Slf4j
@Validated
@RestController
@WithUserContext
@RequiredArgsConstructor
public class SpecificationController implements SpecificationsApi {
    private final SpecificationService specificationService;
    private final SpecificationMapper specificationMapper;

    @Override
    public ResponseEntity<SpecificationDetails> createSpecificationRecord(UUID itemId, SpecificationCreateRequest specificationCreateRequest) {
        log.info("Creating specification for item with id: {}", itemId);
        var accountId = getIdentityOrThrow();
        var dto = specificationMapper.createRequestDto(specificationCreateRequest);
        var details = specificationService.createSpecificationRecord(itemId, dto, accountId);
        log.debug("Created specification with id: {} for item with id: {}", itemId, details.getId());
        return ResponseEntity.ok(details);
    }

    @Override
    public ResponseEntity<SpecificationDetails> updateSpecificationRecord(UUID itemId, UUID specificationId, SpecificationUpdateRequest updateRequest) {
        log.info("Update specification with id: {} for item with id: {}", itemId, specificationId);
        var accountId = getIdentityOrThrow();
        var dto = specificationMapper.updateRequestDto(updateRequest);
        var details = specificationService.updateSpecificationRecord(itemId, specificationId, dto, accountId);
        log.debug("Updated specification with id: {} for item with id: {}", itemId, specificationId);
        return ResponseEntity.ok(details);
    }
}
