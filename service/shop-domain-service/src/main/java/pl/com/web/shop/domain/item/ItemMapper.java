package pl.com.web.shop.domain.item;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;
import pl.com.bit.domain.domain.api.model.ItemCreateRequest;
import pl.com.bit.domain.domain.api.model.ItemDetails;
import pl.com.bit.domain.domain.api.model.ItemUpdateRequest;
import pl.com.bit.domain.domain.api.model.LinkItemRequest;
import pl.com.web.shop.domain.item.model.dto.ItemCreateRequestDto;
import pl.com.web.shop.domain.item.model.dto.ItemUpdateRequestDto;
import pl.com.web.shop.domain.item.model.dto.LinkItemRequestDto;
import pl.com.web.shop.domain.item.model.Item;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    builder = @Builder(disableBuilder = true))
public interface ItemMapper {

    ItemCreateRequestDto createRequestDto(ItemCreateRequest createRequest);

    ItemDetails itemDetails(Item item);

    ItemUpdateRequestDto updateRequestDto(ItemUpdateRequest updateRequest);

    default Page<ItemDetails> toPageItemDetails(Page<Item> itemPage) {
        return itemPage.map(this::itemDetails);
    }

    LinkItemRequestDto linkItemRequestDto(LinkItemRequest request);
}
