package pl.com.web.shop.domain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import javax.annotation.Nullable;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class })
public class ShopDomainServiceApplicationBootstrap {

    public static void main(@Nullable String[] args) {
         SpringApplication.run(ShopDomainServiceApplicationBootstrap.class, args);
    }

}
