package pl.com.web.shop.domain.item.repository;

import com.querydsl.core.types.Predicate;
import pl.com.bit.domain.domain.api.model.ItemSearchFilter;
import pl.com.web.shop.domain.item.model.Item;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

public interface ItemRepositoryCustom {

    Optional<Item> findByIdAndDeletedIsFalse(@NotNull UUID id, @NotBlank String graphName);

    Optional<Item> findByIdAndDeletedIsTrue(@NotNull UUID id);

    Predicate buildPredicate(@NotNull ItemSearchFilter filter);
}
