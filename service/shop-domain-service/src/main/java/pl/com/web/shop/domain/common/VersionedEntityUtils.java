package pl.com.web.shop.domain.common;

import com.querydsl.core.types.dsl.BooleanPath;
import lombok.NoArgsConstructor;
import pl.com.bit.common.querydsl.Predicates;
import pl.com.bit.common.versioned.entity.QVersionedEntity;
import pl.com.bit.domain.domain.api.model.VersionedSearchFilter;

import javax.validation.constraints.NotNull;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class VersionedEntityUtils {

    public static Predicates prepareVersionedFiltersPredicates(@NotNull QVersionedEntity entity,
                                                               @NotNull BooleanPath deleted,
                                                               @NotNull VersionedSearchFilter filter) {
        return Predicates.builder()
            .in(entity.authorId.id, filter.getAuthorIdIn())
            .in(entity.modifierId.id, filter.getModifierIdIn())
            .goe(entity.created, filter.getCreatedGreaterThanOrEqual())
            .loe(entity.created, filter.getCreatedLessThanOrEqual())
            .goe(entity.modified, filter.getCreatedGreaterThanOrEqual())
            .goe(entity.modified, filter.getCreatedGreaterThanOrEqual())
            .eq(deleted, TRUE.equals(filter.getIncludeDeleted()) ? null : FALSE);
    }
}
