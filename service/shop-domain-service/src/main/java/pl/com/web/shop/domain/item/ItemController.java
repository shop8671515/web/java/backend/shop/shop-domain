package pl.com.web.shop.domain.item;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import pl.com.bit.common.security.api.context.WithUserContext;
import pl.com.bit.domain.domain.api.ItemsApi;
import pl.com.bit.domain.domain.api.model.ItemCreateRequest;
import pl.com.bit.domain.domain.api.model.ItemDetails;
import pl.com.bit.domain.domain.api.model.ItemSearchFilter;
import pl.com.bit.domain.domain.api.model.ItemUpdateRequest;
import pl.com.bit.domain.domain.api.model.LinkItemRequest;

import java.util.UUID;

import static pl.com.bit.common.security.api.UserContext.getIdentityOrThrow;

@Slf4j
@Validated
@RestController
@WithUserContext
@RequiredArgsConstructor
public class ItemController implements ItemsApi {
    private final ItemService service;
    private final ItemMapper itemMapper;

    @Override
    public ResponseEntity<ItemDetails> createItem(ItemCreateRequest request) {
        log.info("Creating an item {request: {}}", request);
        var accountId = getIdentityOrThrow();
        var dto = itemMapper.createRequestDto(request);
        var details = service.createItem(dto, accountId);
        log.debug("Created an item {details: {}}", details);
        return ResponseEntity.ok(details);
    }

    @Override
    public ResponseEntity<ItemDetails> updateItem(UUID itemId, ItemUpdateRequest request) {
        log.info("Updating an item {id: {}, request: {}}", itemId, request);
        var accountId = getIdentityOrThrow();
        var dto = itemMapper.updateRequestDto(request);
        var details = service.updateItem(itemId, dto, accountId);
        log.debug("Updated an item {id: {}, details: {}}", itemId, details);
        return ResponseEntity.ok(details);
    }

    @Override
    public ResponseEntity<ItemDetails> getItem(UUID itemId) {
        log.info("Getting an item {id: {}}", itemId);
        var accountId = getIdentityOrThrow();
        var details = service.getItem(itemId, accountId);
        log.debug("Got an item {id: {}, item: {}}", itemId, details);
        return ResponseEntity.ok(details);
    }

    @Override
    public ResponseEntity<Void> deleteItem(UUID itemId) {
        log.info("Deleting an item {id: {}}", itemId);
        var accountId = getIdentityOrThrow();
        service.deleteItem(itemId, accountId);
        log.debug("Deleted an item {id: {}}", itemId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Page<ItemDetails>> getItemDetailsPage(ItemSearchFilter itemsSearchFilter) {
        log.info("Searching for items {filter: {}}", itemsSearchFilter);
        var accountId = getIdentityOrThrow();
        Page<ItemDetails> itemDetails = service.findItems(itemsSearchFilter, accountId);
        log.debug("Found items: {}", itemDetails.getSize());
        return ResponseEntity.ok(itemDetails);
    }

    @Override
    public ResponseEntity<ItemDetails> linkItem(UUID itemId, LinkItemRequest linkItemRequest) {
        log.info("Linking an item of id: {}, with an item of id: {}", itemId, linkItemRequest.getId());
        var accountId = getIdentityOrThrow();
        var dto = itemMapper.linkItemRequestDto(linkItemRequest);
        ItemDetails itemDetails = service.linkItem(itemId, dto, accountId);
        log.debug("Linking an item of id: {}, with an item of id: {}", itemId, linkItemRequest.getId());
        return ResponseEntity.ok(itemDetails);
    }
}
