package pl.com.web.shop.domain.item.repository;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.hibernate.graph.GraphSemantic;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import pl.com.bit.domain.domain.api.model.ItemSearchFilter;
import pl.com.web.shop.domain.common.VersionedEntityUtils;
import pl.com.web.shop.domain.item.model.Item;
import pl.com.web.shop.domain.item.model.QItem;

import javax.persistence.EntityManager;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

import static pl.com.web.shop.domain.item.model.QItem.item;

@Repository
public class ItemRepositoryCustomImpl extends QuerydslRepositorySupport implements ItemRepositoryCustom {
    private final EntityManager entityManager;
    private final JPAQueryFactory queryFactory;

    public ItemRepositoryCustomImpl(EntityManager entityManager) {
        super(QItem.class);
        this.entityManager = entityManager;
        this.queryFactory = new JPAQueryFactory(entityManager);
    }


    @Override
    public Optional<Item> findByIdAndDeletedIsFalse(@NotNull UUID id, @NotBlank String graphName) {
        return Optional.ofNullable(queryFactory.select(item)
                .from(item)
                .where(item.id.eq(id).
                        and(item.deleted.isFalse()))
                .setHint(GraphSemantic.FETCH.getJpaHintName(), entityManager.getEntityGraph(graphName))
                .fetchOne());
    }

    @Override
    public Optional<Item> findByIdAndDeletedIsTrue(UUID id) {
        return Optional.ofNullable(queryFactory.select(item)
                .from(item)
                .where(item.id.eq(id).and(item.deleted.isTrue()))
                .fetchOne());
    }

    @Override
    public Predicate buildPredicate(ItemSearchFilter filter) {
        return VersionedEntityUtils.prepareVersionedFiltersPredicates(item._super, item.deleted, filter)
            .containsIgnoreCase(item.name, filter.getNameContains())
            .containsIgnoreCase(item.description, filter.getDescriptionContains())
            .build();
    }
}
