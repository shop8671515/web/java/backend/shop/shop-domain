package pl.com.web.shop.domain.service.common.ShopRestSpecIT

import lombok.AccessLevel
import lombok.NoArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.test.context.ActiveProfiles
import pl.com.bit.common.security.api.AccountIdentity
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static pl.com.web.shop.domain.service.common.ShopRestSpecIT.ShopRestSpecIT.TestHttpHeadersBuilder.testHttpHeadersBuilder

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
class ShopRestSpecIT extends Specification {
    public static final UUID ACCOUNT_ID = UUID.randomUUID()
    protected static String ACCOUNT_HEADER_NAME = "x-account-id"

    @Autowired
    TestRestTemplate restTemplate

    def <T> ResponseEntity<T> httpPost(String url, Object command, Class<T> responseType, HttpHeaders headers = defaultHeaders(), Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<?>(command, headers), responseType, urlVariables)
    }

    def <T> ResponseEntity<T> httpPut(String url, Object command, Class<T> responseType, HttpHeaders headers = defaultHeaders(), Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<?>(command, headers), responseType, urlVariables)
    }

    def <T> ResponseEntity<T> httpPost(String url, Object command, ParameterizedTypeReference<T> responseType, HttpHeaders headers = defaultHeaders(), Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<?>(command, headers), responseType, urlVariables)
    }

    def <T> ResponseEntity<T> httpGet(String url, Class<T> responseType, HttpHeaders headers = defaultHeaders(), Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<Void>(null, headers), responseType, urlVariables)
    }

    def <T> ResponseEntity<T> httpDelete(String url, Class<T> responseType, HttpHeaders headers = defaultHeaders(), Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<Void>(null, headers), responseType, urlVariables)
    }

    private static HttpHeaders defaultHeaders() {
        testHttpHeadersBuilder().build()
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    static class TestHttpHeadersBuilder {
        private UUID accountId = ACCOUNT_ID
        private String accountRole = 'CLIENT'

        static TestHttpHeadersBuilder testHttpHeadersBuilder() {
            new TestHttpHeadersBuilder()
        }

        TestHttpHeadersBuilder accountId(UUID accountId) {
            this.accountId = accountId
            this
        }

        HttpHeaders build() {
            HttpHeaders headers = new HttpHeaders()
            headers.setAccept([MediaType.APPLICATION_JSON])

            if (accountId) {
                headers.set(ACCOUNT_HEADER_NAME, assembleAccountContextHeader())
            }

            headers
        }

        private String assembleAccountContextHeader() {
            AccountIdentity.builder()
                .id(accountId)
                .authority(new SimpleGrantedAuthority(accountRole))
                .build()
                .toHeaderString()
        }
    }
}
