package pl.com.web.shop.domain.service

import org.spockframework.spring.EnableSharedInjection
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import org.testcontainers.spock.Testcontainers
import pl.com.bit.common.helper.utils.UriHelper
import pl.com.bit.common.named.object.entity.NamedObjectSnap
import pl.com.bit.user.domain.user.api.snap.UserSnapService
import pl.com.bit.user.starter.UserInternalService
import pl.com.bit.user.user.api.model.AccountRole
import pl.com.bit.user.user.api.model.AccountStatus
import pl.com.bit.user.user.api.model.UserExtended
import pl.com.bit.user.user.api.model.UserLite
import pl.com.web.shop.domain.service.common.ShopRestSpecIT.ShopRestSpecIT

import java.time.LocalDate

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@Testcontainers
@Import([UriHelper])
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@EnableSharedInjection
class BaseITSpecification extends ShopRestSpecIT {

    @LocalServerPort
    int port

    @SpringBean
    UserInternalService userInternalService = Mock()

    @Autowired
    UserSnapService userSnapService

    def setup() {
        ServerPortSetup.localPort = port

        userInternalService.getLite(_ as UUID) >> {
            UUID id -> UserLite.builder()
                .accountId(id)
                .name("User ${id}")
                .login("user-${id}")
                .build()
        }

        userInternalService.getExtended(_ as UUID) >> {
            UUID accountId -> UserExtended.builder()
                .accountId(accountId)
                .registrationDate(LocalDate.now())
                .email("test@gmail.com")
                .name("Test${accountId}")
                .lastName("Test${accountId}")
                .status(AccountStatus.ACTIVE)
                .role(AccountRole.CLIENT)
                .login("test")
                .build()
        }
    }

    NamedObjectSnap getUserSnap(UUID id = UUID.randomUUID()) {
        userSnapService.get(id)
    }
}
