from __future__ import annotations

import logging
import os
import subprocess
import sys
from argparse import ArgumentParser, ArgumentTypeError, Namespace

if sys.version_info.minor >= 9:
    from argparse import BooleanOptionalAction
else:
    from argparse import Action

    class BooleanOptionalAction(Action):
        def __init__(
            self,
            option_strings,
            dest,
            default=None,
            type=None,
            choices=None,
            required=False,
            help=None,
            metavar=None,
        ):

            _option_strings = []
            for option_string in option_strings:
                _option_strings.append(option_string)

                if option_string.startswith("--"):
                    option_string = "--no-" + option_string[2:]
                    _option_strings.append(option_string)

            if help is not None and default is not None:
                help += " (default: %(default)s)"

            super().__init__(
                option_strings=_option_strings,
                dest=dest,
                nargs=0,
                default=default,
                type=type,
                choices=choices,
                required=required,
                help=help,
                metavar=metavar,
            )

        def __call__(self, parser, namespace, values, option_string=None):
            if option_string in self.option_strings:
                setattr(namespace, self.dest, not option_string.startswith("--no-"))

        def format_usage(self):
            return " | ".join(self.option_strings)


from itertools import chain
from operator import itemgetter
from pathlib import Path
from typing import Callable, Iterable


def init_logger(level=logging.WARN) -> logging.Logger:
    logger = logging.getLogger(__file__)
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(level)
    logger.addHandler(handler)
    return logger


LOG = init_logger()


def readable_dir_path(dirname: str) -> Path:
    path = Path(dirname)
    if not path.is_dir():
        raise ArgumentTypeError(f"{dirname} nie istnieje albo nie jest katalogiem")
    return path


def readable_file_path(filename: str) -> Path:
    path = Path(filename)
    if not path.is_file():
        raise ArgumentTypeError(f"{filename} nie istnieje albo nie jest plikiem")
    return path


def cli() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("--mark-dirty", action=BooleanOptionalAction, default=True)
    subcommands = parser.add_subparsers(title="polecenia", dest="command")
    subcommands.add_parser("mvn")

    make_parser = subcommands.add_parser("make")
    make_parser.add_argument(
        "-f",
        "--file",
        help="Plik dla którego należy wyliczyć wersję",
        required=True,
        type=readable_file_path,
    )
    make_parser.add_argument(
        "--print-only", action=BooleanOptionalAction, default=False
    )
    return parser.parse_args()


def main():
    args = cli()
    if args.command == "mvn":
        write_maven_versions(args.mark_dirty)
    elif args.command == "make":
        generate_make_version(args.file, args.mark_dirty, args.print_only)
    elif args.command is None:
        for api_name, version in api_versions(args.mark_dirty, all_files_pred):
            print(api_name, version)


def write_maven_versions(mark_dirty: bool):
    print("Generowanie pliku .mvn/maven.config")
    root_dir = Path(__file__).parent.parent
    maven_config = root_dir / "service" / ".mvn" / "maven.config"
    with maven_config.open("w", encoding="utf8") as fp:
        for file_path, version in api_versions(mark_dirty, all_files_pred):
            api_name = removesuffix(Path(file_path).name, ".openapi.yml")
            fp.write(f"-D{api_name}-api.version={version}\n")


def generate_make_version(file: Path, mark_dirty: bool, print_only: bool):
    for filename, version in api_versions(mark_dirty, single_spec_pred(file)):
        version_file = filename + ".VERSION"
        if print_only:
            print(version)
        else:
            with open(version_file, "w", encoding="utf8") as fp:
                fp.write(version)


def all_files_pred(_p: Path) -> bool:
    return True


def single_spec_pred(spec: Path) -> Callable[[Path], bool]:
    spec = spec.absolute()
    return lambda p: not p.name.endswith(".openapi.yml") or p.absolute() == spec


def api_versions(
    mark_dirty: bool, file_pred: Callable[[Path], bool]
) -> Iterable[tuple[str, str]]:
    api_dir = Path(__file__).parent
    root_dir = api_dir.parent
    os.chdir(root_dir)
    src_dir = api_dir / "src"
    shared_files = []
    spec_files = []
    dirty_files = set()
    if mark_dirty:
        dirty_files = set(
            subprocess.run(
                ["git", "diff", "HEAD", "--name-only", str(src_dir)],
                check=True,
                capture_output=True,
            )
            .stdout.decode(sys.stdin.encoding)
            .strip()
            .split("\n")
        )
        dirty_files.discard("")
    for spec in src_dir.glob("*.yml"):
        LOG.debug("Processing %s", spec)
        if not file_pred(spec):
            LOG.debug("Ignoring file %s not matching predicate", spec)
            continue
        version = file_git_version(spec)
        filename = str(spec.relative_to(root_dir))
        if str(spec).endswith(".openapi.yml"):
            spec_files.append((filename, version))
        else:
            shared_files.append((filename, version))

    for item in spec_files:
        filename = item[0]
        postfix = ""
        for shared_file in shared_files:
            if shared_file[0] in dirty_files:
                postfix = "-dirty"
                break
        else:
            if filename in dirty_files:
                postfix = "-dirty"
        max_version = max(chain([item], shared_files), key=itemgetter(1))
        yield (filename, max_version[1] + postfix)


def file_git_version(file: Path) -> str:
    res = subprocess.run(
        [
            "git",
            "log",
            "-n",
            "1",
            "--pretty=format:%ct-%h",
            "--abbrev=10",
            "--",
            str(file),
        ],
        check=True,
        capture_output=True,
    )
    return res.stdout.decode(sys.stdin.encoding).strip()


def removesuffix(text: str, suffix: str) -> str:
    if text.endswith(suffix):
        return text[: -len(suffix)]
    return text


if __name__ == "__main__":
    main()
