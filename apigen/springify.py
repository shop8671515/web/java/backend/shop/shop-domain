from __future__ import annotations

import re
from pathlib import Path

BUILTIN_JAVA_TYPES = {
    "String": None,
    "Integer": None,
    "Long": None,
    "Boolean": None,
    "UUID": "java.util.UUID",
}


def rewrite_api(file: Path):
    with file.open() as fp:
        src = fp.read()
    page_models = search_page.findall(src)
    if not page_models:
        return

    imports = (
        "\nimport org.springframework.data.domain.Page;\n"
        "import org.springframework.data.domain.Pageable;\n"
        "import org.springframework.data.web.PageableDefault;\n"
        "import org.springframework.cloud.openfeign.SpringQueryMap;\n"
    )

    for model_name in page_models:
        class_name, qualified_class_name = get_model_meta(model_name)
        src = src.replace(
            f"ResponseEntity<{model_name}>", f"ResponseEntity<Page<{class_name}>>"
        )
        if qualified_class_name:
            imports += "import " + qualified_class_name + ";\n"

    src = re.sub("(package [^;]+;)", "\\1" + imports, src)
    src = re.sub(
        pageable_regex, "@PageableDefault @SpringQueryMap Pageable pageable", src
    )
    src = re.sub(
        r"\* @param page.+\n\s*\* @param size.+\s*\* @param sort",
        "* @param pageable",
        src,
    )

    with file.open("w+") as fp:
        fp.write(src)

def rewrite_model(file: Path):
    with file.open() as fp:
        src = fp.read()

    # fix escaping of additional annotations
    src = re.sub("asEnum&#x3D;true", "asEnum=true", src)

    with file.open("w+") as fp:
        fp.write(src)


def rewrite_maps_int_key(file: Path, variables: [str]):
    if not file.exists():
        return
    with file.open() as fp:
        src = fp.read()

    property_regex = property_dict_regex(variables)
    src = re.sub(property_regex, "Map<Integer, \\1> \\2", src)
    getter_regex = getter_dict_regex(variables)
    src = re.sub(getter_regex, "public Map<Integer, \\1> \\2", src)
    put_setter_regex = put_dict_regex(variables)
    src = re.sub(put_setter_regex, "\\1(Integer key", src)
    with file.open("w+") as fp:
        fp.write(src)


def property_dict_regex(variables: [str]):
    var = "(" + "|".join(variables) + r")\b"
    return r"Map<String, (.+)> " + var


def put_dict_regex(variables: [str]):
    items = ["put" + v.capitalize() + "Item" for v in variables]
    return "(" + "|".join(items) + r")\(String key"


def getter_dict_regex(variables: [str]):
    getters = ["get" + v.capitalize() for v in variables]
    var = "(" + "|".join(getters) + r")\b"
    return r"public Map<String, (.+)> " + var


def get_model_meta(filename: str) -> tuple[str, str | None]:
    file = list(src_dir.glob(f"**/{filename}.java"))[0]
    with file.open() as fp:
        src = fp.read()
    content_model = search_content.findall(src)[0]
    if content_model in BUILTIN_JAVA_TYPES:
        return (content_model, BUILTIN_JAVA_TYPES[content_model])
    model_file = next(src_dir.glob(f"**/{content_model}.java"))
    return (content_model, ".".join(model_file.parts[3:-1]) + "." + model_file.stem)


src_dir = Path("src")
search_page = re.compile(r"ResponseEntity<(\w+?Page)>")
list_field = re.compile(r"(private List<)")
list_param = re.compile(r"(List<)")
search_content = re.compile(r"private List<(\w+?)> content")
pageable_regex = re.compile(
    r'@ApiParam\(value = "Page number."\).+?Integer page,.+?List<String> sort',
    flags=re.DOTALL,
)

controllers = src_dir.glob("**/*Api.java")
for controller in controllers:
    rewrite_api(controller)

models = src_dir.glob("**/model/*.java")
for model in models:
    rewrite_model(model)
