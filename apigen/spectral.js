// źródło: https://github.com/IBM/openapi-validator/issues/379#issuecomment-1048258333

const ibmRuleset = require('@ibm-cloud/openapi-ruleset');
const { propertyCaseConvention } = require('@ibm-cloud/openapi-ruleset/src/functions');

module.exports = {
    extends: [ibmRuleset],
    rules: {
        'property-case-convention': {
            given: [
                '$.paths[*][parameters][*].schema',
                '$.paths[*][parameters][*].content[*].schema',
                '$.paths[*][*][requestBody].content[*].schema'
                // nazwy typu DMCGreaterThanOrEqual naruszają tę regułę, gdyż to PascalCase; są tylko dwa wystąpienia tego naruszenia
                // '$.paths[*][*][parameters][*].schema',
                // nazwy typu folderSID naruszają tę regułę; występuje kilkanaście naruszeń, ale część z nich to użycie tego samego obiektu w $ref
                // '$.paths[*][*][parameters,responses][*].content[*].schema',
            ],
            then: {
                function: propertyCaseConvention,
                functionOptions: {
                    type: 'camel'
                }
            }
        }
    },
};
